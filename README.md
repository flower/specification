Draft specification for the Flower Programming Language
=======================================================

This is a WIP draft specification / reference manual to the
Flower programming language.

As it is a draft, it is very much subject to change, but as
things stabilise and are tested in an actual compiler they
are slowly written down here.

The specification comes in four separate parts, which
may change independently of each other.

- General definitions and principles (this document)
- [The Flower Language](docs/1.language/1.language.md)
- [The Flower Bytecode Representation](docs/2.bytecode/2.bytecode.md)
- [The Flower Standard Library](docs/3.library/3.library.md)

Licence
-------
The specification documents are licensed under 
[CC-BY-ND-4.0](http://creativecommons.org/licenses/by-nd/4.0/)

Terms and Definitions
---------------------
Across the documents, following definitions are used

- "**Shall**" or "**must**" is to be interpreted as a requirement imposed
on the the implementation or the program.  Conversely "**Shall not**" is to be
interpreted as a prohibition.

- **object** is a region of data storage in the execution environment, that
  represents a single concept and the contents of which represents values
  associated.

- **bit** is unit of data storage in the execution environment, large enough
  to hold an *object* that has one of two values.

- **byte** is an addressable unit of data storage.  Each individual byte of
  an *object* can be addressed uniquely.  A byte is composed of contiguous
  sequence of bits, the number of which is implementation-defined, but
  recommended to be exactly 8.

- **octet** is an *8-bit object*.  It is recommended, but not required that
  implementations treat *octet* and *byte* as synonyms.

- **character** is a single unicode codepoint, encoded in UTF-8 format unless
  another format is explicitly specified.

- **alignment** is a multiplier represented in *bytes* that specifies the minimum
  requirement for an object storage so that an object can only be stored in an
  address that is a multiple of its alignment.  The size of an *object* must be
  a multiple of its alignment.

- **abort** is a process where the execution environment immediately jumps
 into the program teardown step.

- **implementation-defined** means that this document does not specify a detail
  by itself, but requires that implementation both does so, and provides a way
  for the user to retrieve that information.

- **undefined** means that neither this documentation or the implementation will
  provide no definition for the detail.  Programs relying on these details are
  explicitly *non-conforming*.

Implementation Conformance
--------------------------
The semantic descriptions in these documents define an abstract machine.  The
documents place no requirements on the structure of conforming implementations.
In particular, they need not copy or emulate the structure of the abstract machine.
Rather, conforming implementations are required to emulate only the observable
behaviour of the abstract machine.

Some details of the abstract machine are described in these documents as
implementation-defined.  Each implementation must include documentation describing
its charateristics and behaviour in these respects.  Such documentation shall define
the instance of the abstract machine corresponding to that implementation.

An implementation is free to disregard *any requirement* of these documents, as long
as the result is as if the requirement had been obeyed, as far as can be determined
from the observable behaviour of the program.  For instance, this allows implementations
to skip evaluating part of an expression if it can deduce that its value is not used and
that no side effects affecting the observable behaviour of the program are produced.

Conforming implementations must
  - Produce the same observable behaviour with the same program as the abstract machine
    with same inputs would
  - Evaluate volatile accesses strictly according to the rules of the abstract machine
  - Ensure that at program termination, all data written into files (which might be
    physical devices) shall be identical to the result that execution of the program
    according to the abstract semantics would have produced
  - Ensure that the input and output dynamics of interactive devices shall take place
    in such a fashion that prompting output is delivered before a program waits for
    input.  What constitutes an interactive device is implementation-defined

Translation of source code
--------------------------
A Flower program consists of one or more *source segments* which are provided
to the translation phase.  The implementation must accept the source segments
to be provided as files, and may allow other ways to provide them.

The source segments are represented as UTF-8 encoded text.

A *source segment* is a part of exactly one *module*.  All *source segments*
combined comprise a *module*.  A complete collection of *source segments* to
be included in a *module* forms a *translation unit*.  Separate translation
units communicate by using their *exported interface*.

As long as the source segments remain unchanged, the translated module may be
used as-is, without repeating the translation step.

### Phases of translation
The implementation behaves as if these separate phases would occur
TODO:
  1. Preprocessing of source segments (combining literals, assigning translation units)
  2. Forming semantic tree / graph from the source code
  3. Starting from entry functions, translate to FIR, evaluating compile-time functions
  4. Save bytecode output
  5. Possibly exit if bytecode output was all that was requested
  6. ????
  7. Functioning native program

