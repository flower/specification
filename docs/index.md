Flower language specifications
==============================

This site contains documentation of the Flower programming language.
It is separated into three separate parts, [language](1.language/1.language.md),
[bytecode](2.bytecode/2.bytecode.md) and [library](3.library/3.library.md).  Each
of these can be concidered its own specification that together form the full
Flower language specification.

- The [language](1.language/1.language.md) specifies the core language concepts
- The [bytecode](2.bytecode/2.bytecode.md) specifies the Flower bytecode format that
  is used internally by the compiler and as Flower binary format from modules.
- The [library](3.library/3.library.md) specifies the components of the Flower standard
  library.


Flower language goals and non-goals
-----------------------------------

### Language design
Flower aims to be a general-purpose programming language, with a bias towards
systems programming and scientific programming.  It goal is not to replace other
languages, but to play into its own strengths.  It follows in the footsteps of
C++ and Rust, using RAII as its method of choice for resource and lifetime
management, while aspiring to be much smaller in scope than either of those
languages.

In short, Flower aims to be simple-ish low-level general-purpose programming
language with consistency, interoperability and clarity.


#### Consistency
Flower follows *the principle of least surprise*.  While it may welcome experimental
features, the behaviour of a flower program should not be surprising to its reader.

This calls for both internal consistency within the language and external consistency
among Flower programs.  This makes both maintenance of the tools and programs easier,
and comprehending the aspects of the language and its tools faster.


#### Stability and interoperability
The language should guide its user with the conventions of the language, and provide
ways for external programs to more easily interoperate with each other.

Flower should not be afraid to be a bit experimental here and there, and it should
keep the option for its implementations to change a open as reasonable.  But it also
should provide stability where change is unnecessary.  Most importantly, those who
use Flower should easily be able to know they can rely on its interfaces, ABI, or
definitions being stable.

Similarly, Flower developers should know from the start whether something is a feature
or a bug.  Most of functionality in Flower, all the way to the ABI level should be specified
in enough detail that two Flower implementations produce interoperable results.  This also
allows Flower itself to be tested to a greater degree and allows alternate implementations
of the language to exist without needing to argue which implementation is right.


#### Complexity
Flower aims for *medium complexity*.  It should not be another Rust or C++, but its
goal is not to be another Python either.  The goal is to not have complexity be a
blocker for a feature, if that feature is deemed practical in Flower's domain, but
not be any more complex than necessary.  Flower will not be as easy to approach as
Python, but its learning curve should be kept at a manageable level.


#### Aim for safe defaults, but give way *when* the programmer wants to stupid things
Doing something the right way should be easier than doing it the stupid way.
Defaults should be trustworthy and reasonable.


Terms used
----------
Across the documents, following definitions are used

- "**Shall**" or "**must**" is to be interpreted as a requirement imposed
  on the the implementation or the program.  Conversely "**Shall not**" is to be
  interpreted as a prohibition.

- **object** is a region of data storage in the execution environment, that
  represents a single concept and the contents of which represents values
  associated.

- **bit** is unit of data storage in the execution environment, large enough
  to hold an *object* that has one of two values.

- **byte** is an addressable unit of data storage.  Each individual byte of
  an *object* can be addressed uniquely.  A byte is composed of contiguous
  sequence of bits, the number of which is implementation-defined, but
  recommended to be exactly 8.

- **octet** is an *8-bit object*.  It is recommended, but not required that
  implementations treat *octet* and *byte* as synonyms.

- **character** is a single unicode codepoint, encoded in UTF-8 format unless
  another format is explicitly specified.

- **alignment** is a multiplier represented in *bytes* that specifies the minimum
  requirement for an object storage so that an object can only be stored in an
  address that is a multiple of its alignment.  The size of an *object* must be
  a multiple of its alignment.

- **abort** is a process where the execution environment immediately jumps
 into the program teardown step.

- **implementation-defined** means that this document does not specify a detail
  by itself, but requires that implementation both does so, and provides a way
  for the user to retrieve that information.

- **undefined** means that neither this documentation or the implementation will
  provide no definition for the detail.  Programs relying on these details are
  explicitly *non-conforming*.

- **planned feature** means that there is not even experimental implementation
  for the feature, and the documentation about the subject should not be
  considered normative.

- **experimental** means that there is at least one experimental implementation
  of the feature, but heavy changes are still to be expected.  Experimental features
  are *optional* to implement.

- **unstable** means that the feature exists and is in use, but it has either not
  received enough testing or the feature is such that its implementation is deemed to
  possibly be subject to a change in the future, where the change might be at ABI level.
  Unstable features are ready-for-use and a conforming implementation must implement them,
  a feature should be considered unstable unless otherwise noted.

- **stable** means that the feature will not receive backwards-incompatible changes.
  This includes ABI stability.  A later version of the language may deprecate the feature,
  and deprecated features can be removed by even later version of the language, but for
  most intents and purposes, stable features are set in stone.


Implementation Conformance
--------------------------
The semantic descriptions in these documents define an abstract machine.  The
documents place no requirements on the structure of conforming implementations.
In particular, they need not copy or emulate the structure of the abstract machine.
Rather, conforming implementations are required to emulate only the observable
behaviour of the abstract machine.

Some details of the abstract machine are described in these documents as
implementation-defined.  Each implementation must include documentation describing
its charateristics and behaviour in these respects.  Such documentation shall define
the instance of the abstract machine corresponding to that implementation.

An implementation is free to disregard *any requirement* of these documents, as long
as the result is as if the requirement had been obeyed, as far as can be determined
from the observable behaviour of the program.  For instance, this allows implementations
to skip evaluating part of an expression if it can deduce that its value is not used and
that no side effects affecting the observable behaviour of the program are produced.

Conforming implementations must

  - Produce the same observable behaviour with the same program as the abstract machine
    with same inputs would
  - Evaluate volatile accesses strictly according to the rules of the abstract machine
  - Ensure that at program termination, all data written into files (which might be
    physical devices) shall be identical to the result that execution of the program
    according to the abstract semantics would have produced
  - Ensure that the input and output dynamics of interactive devices shall take place
    in such a fashion that prompting output is delivered before a program waits for
    input.  What constitutes an interactive device is implementation-defined


Translation of source code
--------------------------
A Flower program consists of one or more *source segments* which are provided
to the translation phase.  The implementation must accept the source segments
to be provided as files, and may allow other ways to provide them.

The source segments are represented as UTF-8 encoded text.

A *source segment* is a part of exactly one *module*.  All *source segments*
combined comprise a *module*.  A complete collection of *source segments* to
be included in a *module* forms a *translation unit*.  Separate translation
units communicate by using their *exported interface*.

As long as the source segments remain unchanged, the translated module may be
used as-is, without repeating the translation step.

### Phases of translation
The implementation behaves as if these separate phases would occur
TODO:

  1. Preprocessing of source segments (combining literals, assigning translation units)
  2. Forming semantic tree / graph from the source code
  3. Starting from entry functions, translate to FIR, evaluating compile-time functions
  4. Save bytecode output
  5. Possibly exit if bytecode output was all that was requested
  6. ????
  7. Functioning native program

