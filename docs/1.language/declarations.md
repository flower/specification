Definitions
===========

A *definition* introduces a language entity, and defines their properties.  It may
associate them with a name.  Only one definition of an entity may be given.

Grammar
-------
### Tags

Definitions can have *tags* associated with them.  The tags provide extra information,
such as alignment restrictions, used ABIs, operator precedences and other various data.

```flower-ebnf
tag_name    ::= (identifier | implementation-id '::' identifier)
tag-value   ::= identifier | keyword
tag         ::= tag-name [':' tag-value]
tag-list    ::= '[' tag [',' tag]* ']'
```

If the `tag_name` is in the `implementation-id '::' identifier*` format, the tag is
called an *implementation-specific tag*.


### Generic parameters


### Requirements

A *requirement* is a constraint to a *generic parameter*. 


```flower-ebnf
type_requirement ::= identifier ':' concept
requirement      ::= type_requirement
```


General definition syntax and rules
-----------------------------------

General Flower definition syntax is as follows

```flower-ebnf
definition ::= [ tag-list ] 'def' identifier [ requirement-list ] ':' type (('=' value) | block)
```

An alternative syntax is also allowed, where requirements are placed at later point of the definition,
which is recommended when the number of requirements would make the definition difficult to read
otherwise.

```flower-ebnf
definition ::= [ tag-list ] 'def' identifier [ generic-param-list ] ':' type
               ['with' requirement-list ] (('=' value) | block)
```

The definitions must follow the following rules:

- If a tag is encountered by a compiler that is neither listed in the "allowed tags" section below for
  that kind of definition nor an *implementation-defined tag*, the program is ill-formed.
- If conflicting tags are encountered by a compiler, the program is ill-formed.

Function definition
-------------------
A *function definition* is a *definition*, where the type is a [function type](functions.md).  If a
function is defined as another function using the `= value` syntax, this is called a *function alias*


### Allowed tags

| **Tag**   | **Default** | **Description**                                                     |
| ---       | ---         | ---                                                                 |
| abi       | auto        | function ABI, describes calling convention, name mangling, etc.        |
| export    | -           | marks the function to be exported, exported functions are always <br> generated, even if they are never used |

### Examples

- Defining a function, with the name `main`, that has no input parameters and returs a variable of type `s32`

```flower
def main : func () -> s32 {
    return 42;
}
```

- Defining a generic function, with the generic parameter `T` that is required to fulfill `std::Arithmetic`.
  The function has the name `sum`, takes two parameters of type `T` and returns a variable of type `T`.

```flower
def sum(T: std::Arithmetic) : func (lhs: T, rhs: T) -> T {
    return lhs + rhs;
}
```

the definition can be written in alternate form as 

```flower
def sum(T) : func (lhs: T, rhs: T) -> T
with T: std::Arithmetic
{
    return lhs + rhs;
}
```

- Overloading/specialising a generic function.  Here the generic type is replaced with `string` type.

```flower
def sum(string) : func (lhs: string, rhs: string) -> string {
    return std::concat(lhs, rhs);
}
```

Operator definition
-------------------
!!! warning

    operator definitions are a *planned feature*

An *operator definition* is a *definition*, where the type is a [operator type](operators.md).  Operator
definitions follow the same pattern as *function definitions*.  Custom operators are treated as function
calls, so they have similar properties.

### Allowed tags

The first value for a tag is the default.  If value field is marked with a dash (-), the tag
does not take a value parameter.

| **Tag**     | **Default** | **Description**                                                     |
| ---         | ---         | ---                                                                 |
| abi         | auto        | function ABI, describes calling convention, name mangling, etc.        |
| export      | -           | marks the function to be exported, exported functions are always <br> generated, even if they are never used |


Value definition
----------------
A *value definition* is a *definition*, where the type is a [value type](values.md).


Type definition
---------------
A *type definition* is a *definition*, where the type is a [type](types.md).


