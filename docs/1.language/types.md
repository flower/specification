Types
=====

`integer` type
--------------
Flower provides language `integer` type that represents a variable-sized (bigint)
integer type.  It is the default integer type that is used with integer literals.

Implicitly convertible to integer types that can be deemed to fit the possible
values in compile-time.  An integer internal representation is defined in the
[bytecode](../2.bytecode/2.bytecode.md) document.

!!! note

    As integer type is implicitly convertible to fixed-size integers that they
    can be deemed to fit, statements such as `def num: u32 = 531` automatically
    assign the type without need of explicit cast.


!!! warning

    Range-checking is a *planned feature*


Fixed-size integer types
------------------------
Flower supports both unsigned and signed fixed width integers.

| Length    | Signed    | Unsigned      |
| ---       | ---       | ---           |
| 8-bit     | `s8`      | `u8`          |
| 16-bit    | `s16`     | `u16`         |
| 32-bit    | `s32`     | `u32`         |
| 64-bit    | `s64`     | `u64`         |
| 128-bit   | `s128`    | `u128`        |

While the bit size is usually $2^n$, Flower does support non-power-of-two length
integers, so `s42` is a valid signed 42-bit value.  The length part can be any
number between 2 and 1024.

Signed fixed-size-integers can store numbers from $-(2^{n-1})$ to $2^{n-1} - 1$.
Unsigned fixed-size-integers can store numbers from $0$ to $2^n - 1$.


### Overflow behaviour

Both signed and unsigned integers wrap on overflow by default.  Signed integers
use two's complement internally.

!!! warning

    overflow behaviour is *experimental feature*


### Bitwise operations

Only unsigned integer types support bitwise operations.


### Conversions

Fixed-size integer types must be typecast to other integer types, no implicit
conversions take place in any circumstances.


Boolean type
------------

`none` type
-----------

Floating point types
--------------------

variant types
-------------
Flower supports language-level variants.  These are also called sum types
or `or` types or tagged unions.



Function types
--------------

Object types
------------
Flower supports structs or product types.

!!! warning

    Object types are a *planned feature*


`type` type
-----------
