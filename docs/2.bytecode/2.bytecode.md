The Flower Bytecode Representation
==================================
This document describes the Flower Bytecode format and the
Flower Intermediate Representation (FIR).  It also serves as a
guide to development of virtual machines capable of running
Flower bytecode, which is required from any conforming
Flower compiler in order to support the compile-time operations
of the language.

Overall structure
=================
Basically, a FIR program is merely a collection of functions and
associated metadata.  There is no specified text format for FIR.

Types
=====

Base Types
----------
### none
None is a use of undefined type, but one that is possible for the
virtual machine to deduce without causing an error.

### fixed-size signed integer
A two's complement fixed-size integer number

### fixed-size unsigned integer
### boolean
### floating point number
### variable-sized integer
### variant
### function
### object
### type
### invalid
Invalid base type is a type error.  If the virtual machine encounters such
value, it must report an error and abort instantly.  A well-formed
FIR program will never have these, but they can be useful at debugging.

Bytecode Instructions
=====================

### NOOP
### HALT
### ZERO

### ADD
### SUB
### MUL
### DIV

### ALLOC
### STORE
### LOAD
### INL

### BAND
### BOR
### BXOR
### BNOT
### SHR
### SHL
### ROR
### ROL

### EQ
### NEQ

### CALL
### CAST

### JUMP
### JNZ
### JNE
### RET
